var router = require('express').Router();
var authController = require('../controllers/authentication');

router.post('/auth/facebook', authController.facebook);
router.post('/auth/github', authController.github);

module.exports = router;